﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using UmbracoLearning.Models;
using Umbraco.Core.Models;

namespace UmbracoLearning.Controllers
{
    public class SiteSharedLayoutController : SurfaceController
    {
        private const string Partial_Views_Path = "~/Views/Partials/SharedLayout/";
        public ActionResult Header()
        {
            List<NavigationList> nav = GetNavigationModel();
            return PartialView($"{Partial_Views_Path}Header.cshtml",nav);
        }
        public ActionResult Footer()
        {
            return PartialView($"{Partial_Views_Path}Footer.cshtml");
        }

        //Step 1: create function to get the navigation model from database
        //Step 2: write the code to get the sub navigation
        //Step 3: update the code for header

        public List<NavigationList> GetNavigationModel()
        {
            int pageId = int.Parse(CurrentPage.Path.Split(',')[1]); // There is the page position in path
            IPublishedContent pageInfo = Umbraco.Content(pageId);
            var nav = new List<NavigationList>
            {
                new NavigationList(new NavigationLinkInfo(pageInfo.Url,pageInfo.Name))
            };
            nav.AddRange(GetSubNavigationList(pageInfo));
            //add range of nav(parent & child)
            return nav;
        }

        public List<NavigationList> GetSubNavigationList(dynamic page)
        {
            List<NavigationList> navList = null;
            var subPages = page.Children.Where("Visible");
            if (subPages != null && subPages.Any() && subPages.Count() > 0)
            {
                navList = new List<NavigationList>();
                foreach (var subPage in subPages)
                {
                    var listItem = new NavigationList(new NavigationLinkInfo(subPage.Url, subPage.Name))
                    {
                        NavItems = GetSubNavigationList(subPage)
                    };
                    navList.Add(listItem);
                }
            }
            return navList;
        }

    }
}
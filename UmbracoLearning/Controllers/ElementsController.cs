﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace UmbracoLearning.Controllers
{
    public class ElementsController: SurfaceController
    {
        private const string Partial_Views_Path = "~/Views/Partials/Elements/";
        public ActionResult ElementsMainContent()
        {
            return PartialView($"{Partial_Views_Path}ElementsMainContent.cshtml");
        }
    }
}
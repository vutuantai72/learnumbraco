﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace UmbracoLearning.Controllers
{
    public class GenericController: SurfaceController
    {
        private const string Partial_Views_Path = "~/Views/Partials/Generic/";
        public ActionResult GenericMainContent()
        {
            return PartialView($"{Partial_Views_Path}GenericMainContent.cshtml");
        }
    }
}
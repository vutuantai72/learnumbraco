﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace UmbracoLearning.Controllers
{
    public class HomeController: SurfaceController
    {
        private const string Partial_Views_Path = "~/Views/Partials/Home/";
        public ActionResult Banner()
        {
            return PartialView($"{Partial_Views_Path}Banner.cshtml");
        }

        public ActionResult SectionOne()
        {
            return PartialView($"{Partial_Views_Path}SectionOne.cshtml");
        }
        public ActionResult SectionTwo()
        {
            return PartialView($"{Partial_Views_Path}SectionTwo.cshtml");
        }
        public ActionResult SectionThree()
        {
            return PartialView($"{Partial_Views_Path}SectionThree.cshtml");
        }
    }
}